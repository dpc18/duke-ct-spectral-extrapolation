
# Apply the trained spectral extrapolation model to kidney lesion data sets.

if True:

    import os

    # os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"

    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()

    from tensorflow import keras

    from pptx      import Presentation
    from pptx.util import Inches, Pt
    from pptx.enum.shapes import MSO_SHAPE
    from pptx.dml.color import RGBColor

    import xlsxwriter

    from datetime import datetime

    import re

    from Regina_Schwartz_et_al_2021_Renal_Lesions.Duke_S_Extrap_Apply_Support import get_coordinates, get_circle_mask, get_oval_mask, make_diam_rmask0, make_diam_rmask, \
                                                                                     mask_distance, mat_decomp2, PiecewiseLinearTransfer, make_G_2D_sep, fill_B, list_gen_v4

    import numpy as np

    import glob

    import pydicom

    from tensorflow.python.keras.layers import Conv2D, Activation, UpSampling2D, LeakyReLU

    from scipy import misc

    np.random.seed(seed=317)

    # from PIL import Image


## Network parameters

    # These should be divisible for feed through
    batch_size  =  4 # 8 # 16
    avg_slices  =  4 # number of 0.75 mm slices to average

    accum_iters =   1 # 1 # 8  # 16
    epochs      = 100 # 100
    do_train    = True # False

    decay_factor = 10  # decay by one order of magnitude over training
    # decay_factor = 100  # decay by one order of magnitude over training

    fs = 3 # 4 # 5

    sh0 = [768,768]

    target_slice_thickness = 3.0
    target_diameter        = 500.0
    current_pix_size       = 512

if True:

## Data sets

    data_base = '/media/justify/DPC/Duke_S_Extrap_2019/Kidney_lesions_DE_Flash_3_6_20/'

    # NOTE: [] are reserved characters in glob expressions
    data_ = 'A?B/*'

    kidney_slices = [76,93,87,400,34,84,68,159,169,138,222,138,69,113,177,167,142,158,119,198,187,123,184,188,205,254,159,159,200,185,169,157,167,117,167,201,170,127,63,129,137,212,87,122,110,209,73,27,95,197,130,186,235,167,147,191,136,144,136,269,191,236,136,131,149,179,201,201,130,193,198,126,191,106,117,115,156,191,143,130,82,173,255,124,142,151,136,161,195,169,244,159,209,229,91,100,201,157,108,191,145,203,82,98,174,242,135,216,76,91,184,111,139,193,105,165,233,134,190,111,134,117,144,223,148,151,120,151,164,169,162,200,97,190,101,140,133,175,175,121,131,95,128,185,196,175,177,241,181,177,122,221,158,203,155,155,154,229,98,126,181,160,220,149,141,148,156,199,196,180,172,202,112,41,39,72,40,124,49,30,55,56,59,79]

    A_list = []
    B_list = []

    total_slices = 0

    all_cases = []

    check_header    = True # check the consistency of dicom headers / image parameters across cases
    voxel_size_x    = []
    voxel_size_y    = []
    slice_thickness = []
    conv_kernel     = []
    diam            = []
    kVpA            = []
    kVpB            = []

    # Get all case directories
    # Treat multiple scans from the same patient as two scans => NOT multiple scans, max 500 slices / subfolder
    # ignore "TNC" scans
    cases = []
    for root,dirs,files in os.walk(data_base):

        if not dirs and not 'TNC' in root:

            cases.append(root)

    cases = sorted(cases)

    case_names = [re.search('/A(.+?)/',cases[i]).group(0)[1:-1] for i in range(0,len(cases))]


    # Collect data from each case
    for j in range(0,len(cases)):

        case_ = glob.glob( cases[j] + '/*' )

        case_ = sorted( case_ )

        assert len(case_) % 2 == 0

        A_list.append( case_[0:len(case_)//2] )
        B_list.append( case_[len(case_)//2:] )

        total_slices += len(case_)//2

        if check_header:

            tempA = pydicom.dcmread(case_[0])
            tempB = pydicom.dcmread(case_[len(case_)//2])

        # Check to make sure the A and B data is parsed correctly

            assert 'DET_A' in tempA.ImageType and 'DET_B' in tempB.ImageType
            assert tempA.InstanceNumber == 1 and tempB.InstanceNumber == 1

        # Record important parameters

            diam.append( tempA.ReconstructionDiameter )

            voxel_size_x.append( tempA.PixelSpacing[0] )

            voxel_size_y.append( tempA.PixelSpacing[1] )

            conv_kernel.append( tempA.ConvolutionKernel[0] )

            slice_thickness.append( tempA.SliceThickness )

            kVpA.append( tempA.KVP )
            kVpB.append( tempB.KVP )

    print('Total slices: ' + str(total_slices))

    vox_size  = np.stack( (np.array(voxel_size_x),np.array(voxel_size_y)), axis=1 )
    kVps      = np.stack( (np.array(kVpA),np.array(kVpB)), axis=1 )
    slc_th    = np.array(slice_thickness)
    diams     = np.array(diam)

    # Remove data which does not have a 1.5 or 3.0 mm slice thickness
    # Remove data which did not use the Q30f kernel for reconstruction
    case_idx = []
    case_rm  = []
    for j in range(0,len(cases)):

        if (slc_th[j] not in [1.5,3.0]) or (conv_kernel[j] not in ['Q30f']) or (kVps[j,0] != 100) or (kVps[j,1] != 140):

            print('Removing case ' + case_names[j])

            case_rm.append(j)

        else:

            case_idx.append(j)

    case_idx = np.array(case_idx,dtype=np.uint32)

    vox_size    = vox_size[case_idx,:]
    kVps        = kVps[case_idx,:]
    slc_th      = slc_th[case_idx]
    diams       = diams[case_idx]
    conv_kernel = [ conv_kernel[i] for i in case_idx ]

    A_list = [ A_list[i] for i in case_idx ]
    B_list = [ B_list[i] for i in case_idx ]

    kidney_slices = [ kidney_slices[i] for i in case_idx ]

    cases      = [ cases[i] for i in case_idx ]
    case_names = [ case_names[i] for i in case_idx ]

    case_list = []
    th_list   = []
    diam_list = []
    for j in range(0,len(cases)):

        case_list.append( [ case_names[j] ] * len(A_list[j]) )
        th_list.append(   [ slc_th[j]     ] * len(A_list[j]) )
        diam_list.append( [ diams[j] ]      * len(A_list[j]) )

    del voxel_size_x, voxel_size_y, kVpA, kVpB, slice_thickness, diam, tempA, tempB # , case_names


'''
Take measurements based on filled ROIs drawn on a PowerPoint slide
'''

if True:

    # size of the images
    top_p   = 914400
    left_p1 = 609480
    left_p2 = 6095880

    # number of points in 50 cm
    hw = left_p2 - left_p1

    pts_mm  = hw / target_diameter
    pix_pts = sh0[0] / hw

    x1,x2 = get_coordinates( nx=sh0[0], ny=sh0[0], batch_size=batch_size )

    # Note: This file of lesion dilneations is not included in this repository.
    ROI_PP = '/media/justify/DPC/Duke_S_Extrap_2019/Kidney_lesions_DE_Flash_3_6_20/DS_DE_Kidney_Lesions_March_2020_Revised_Masks_ROIs_Fides.pptx'

    prs = Presentation(ROI_PP)

    # Stride between slides potentially containing ROIs
    slide_stride = 3

    c  = -1
    c2 = -1

    for slide in prs.slides:

        c += 1

        if c % slide_stride != 0:

            continue

        c2 += 1

        if c2 == 0:

            rmasks   = [[]]
            rrots    = [[]]
            rheights = [[]]
            rwidths  = [[]]
            rlefts   = [[]]
            rtops    = [[]]

        else:

            rmasks.append( [] )
            rrots.append( [] )
            rheights.append( [] )
            rwidths.append( [] )
            rlefts.append( [] )
            rtops.append( [] )

        sc = 0

        for shape in slide.shapes:

            if shape.shape_type == 1:

                try:

                    st = shape.auto_shape_type # oval

                except:

                    continue

                if st == 9:

                    sc += 1

                    rmasks[c2].append( get_oval_mask(x1,x2,topROI=pix_pts*(shape.top-top_p),leftROI=pix_pts*(shape.left-left_p1),widthROI=pix_pts*(shape.width),heightROI=pix_pts*(shape.height),rotation=shape.rotation) )

                    rrots[c2].append( shape.rotation )
                    rheights[c2].append( pix_pts*shape.height )
                    rwidths[c2].append(  pix_pts*shape.width )
                    rlefts[c2].append(   pix_pts*shape.left )
                    rtops[c2].append(    pix_pts*shape.top )

                    # make sure we really have a perfect circle and not an arbitrary oval
                    # assert shape.width == shape.height

                    # masks[c].append( get_circle_mask(x1,x2,topROI=pix_pts*(shape.top-top_p),leftROI=pix_pts*(shape.left-left_p1),widthROI=pix_pts*(shape.width)) )

                    # print('type (%s): %d, top: %d, left: %d, width: %d, height: %d, rotation: %f' % (shape.name,shape.shape_type,shape.top,shape.left,shape.width,shape.height,shape.rotation))


'''
Export kidney lesion ROI positions for improved mask placement
Choose mask size to just intersect the closest ROI
'''

if True:

    mm_pix    = (50*10)/768
    mask_diam = 200

    # The first of every three slides has one or more ROIs drawn around kidney lesions
    # Note: This file of lesion dilneations is not included in this repository.
    ROI_PP = '/media/justify/DPC/Duke_S_Extrap_2019/Kidney_lesions_DE_Flash_3_6_20/Freehand_ROI_DS_DE_Kidney_Lesions_March_2020.pptx'

    prs = Presentation(ROI_PP)

    slides = prs.slides
    c = 0

    # size of the images
    top_p   = 914400
    left_p1 = 609480
    left_p2 = 6095880

    # number of points in 50 cm
    hw = left_p2 - left_p1

    pts_mm = hw / target_diameter

    mask_centers = {} # []
    mask_diams   = {} # []

    for slide_idx in range(0,393,3):

        slide = slides[slide_idx]

        c += 1

        # print( '==> %s:' %c  )

        # find all valid ROIs and their positions
        ROI_tops    = []
        ROI_lefts   = []
        ROI_widths  = []
        ROI_heights = []
        for shape in slide.shapes:

            # The ROIs are picture objects (13)
            # The actual pictures have known positions, look at other positions
            if shape.shape_type == 13 and shape.top != top_p:

                ROI_tops.append( shape.top - top_p )
                ROI_lefts.append( shape.left - left_p1 )
                ROI_widths.append( shape.width )
                ROI_heights.append( shape.height )

        # If we found an ROI(s) on this slide, compute mask center, radius
        mask_center = [target_diameter/2.0,target_diameter/2.0] # xm0,ym0 mm
        ROI_topsN    = np.array( ROI_tops, np.float64 )    / pts_mm
        ROI_leftsN   = np.array( ROI_lefts, np.float64 )   / pts_mm
        ROI_widthsN  = np.array( ROI_widths, np.float64 )  / pts_mm
        ROI_heightsN = np.array( ROI_heights, np.float64 ) / pts_mm

        pbx = []
        pby = []
        for i in range(0,len( ROI_tops )):

            pbx.append([ROI_leftsN[i] / mm_pix, (ROI_leftsN[i] + ROI_widthsN[i])  / mm_pix ])
            pby.append([ ROI_topsN[i] / mm_pix, (ROI_topsN[i]  + ROI_heightsN[i]) / mm_pix ])

        # mask = make_diam_rmask0( nx=sh0[0], ny=sh0[1], px=mask_center[0] / mm_pix, py=mask_center[1] / mm_pix, pbx=pbx, pby=pby, diam=mask_diam/mm_pix )

        # plt.figure(num=1)
        # plt.imshow( mask, cmap='gray', vmin=0, vmax=2 )
        # plt.show(block=False)
        # plt.pause(2.0)


        if len( ROI_tops ) > 0:

        # 0) Compute points on the box closest to the center of the mask: xb, yb

            xb0 = ROI_leftsN + ROI_widthsN/2.0
            yb0 = ROI_topsN  + ROI_heightsN/2.0

            xb = xb0 + np.sign( mask_center[0] - xb0 ) * ROI_widthsN / 2.0

            dxb = np.sign( mask_center[0] - xb0 ) * ROI_widthsN / 2.0
            dyr = mask_center[1] - yb0
            dxr = mask_center[0] - xb0

            yb  = yb0 + (dxb * dyr) / dxr

            dist = np.sqrt( ( mask_center[0] - xb )**2 + ( mask_center[1] - yb )**2 )

        # 1) shift the mask to intersect the closest ROI

            cidx = np.argmin(dist)

            x0_y0 = 1.0

            yn = np.sqrt( ( ( mask_diam / 2.0 )**2 ) / 2 )
            xn = x0_y0 * yn

            xm = xb[cidx] + np.sign( mask_center[0] - xb[cidx]  ) * xn
            ym = yb[cidx] + np.sign( mask_center[1] - yb[cidx]  ) * yn

        # 2) shrink the mask to avoid other ROIs

            dist = np.sqrt( ( xm - xb )**2 + ( ym - yb )**2 )

            diam = mask_diam

            if np.amin( dist ) < (mask_diam/2.0)*0.9999:

                diam = 2.0*np.amin(dist)

        # 3) store values

            mask_centers[case_names[c-1]] = [ xm, ym ]
            mask_diams[case_names[c-1]]   = diam

            # mask_centers.append( [ xm, ym ] )
            # mask_diams.append( diam )

        # 4) diagram results

            # mask = make_diam_rmask0( nx=sh0[0], ny=sh0[1], px=mask_centers[-1][0] / mm_pix, py=mask_centers[-1][1] / mm_pix, pbx=pbx, pby=pby, diam=mask_diams[-1]/mm_pix )
            #
            # plt.figure(num=1)
            # plt.imshow( mask, cmap='gray', vmin=0, vmax=2 )
            # plt.show(block=False)
            # plt.pause(2.0)

    # plt.close('all')



## Create a new list of the kidney slices

    A_list_sub    = []
    B_list_sub    = []
    case_list_sub = []
    th_list_sub   = []
    diam_list_sub = []

    for j in range(0,len(cases)):

        # clen  = len( A_list[j] )
        slc_idx = kidney_slices[j] - 1 # image numbers start at 1
        th      = th_list[j][0]

        if th == 1.5:

            bz = batch_size * 2

        else: # assume 3.0

            bz = batch_size

        A_list_sub.append( A_list[j][ slc_idx:slc_idx+bz ] )
        B_list_sub.append( B_list[j][ slc_idx:slc_idx+bz ] )
        case_list_sub.append( case_list[j][ slc_idx:slc_idx+bz ] )
        th_list_sub.append(   th_list  [j][ slc_idx:slc_idx+bz ] )
        diam_list_sub.append( diam_list[j][ slc_idx:slc_idx+bz ] )


# Paths

    dbase   = '/media/x-ray/blkbeauty4/Convolutional_denoising/CNN_denoising/'

    outpath_tf   = '/media/x-ray/blkbeauty4/Duke_S_Extrap_2019_Results/TF_Metrics/'

    outpath_base = '/media/x-ray/blkbeauty4/Duke_S_Extrap_2019_Results/Outputs/'

    run_name     = 'Duke_S_Extrap_2020_apply_v23'

    path = os.path.join(outpath_base + run_name + '_{}'.format(datetime.strftime(datetime.now(), '%Y_%m_%d_%H-%M-%S')))

    if not os.path.isdir(path):
        os.mkdir(path)

    model1_name    = os.path.join(path, run_name + '_model1.h5')
    model2_name    = os.path.join(path, run_name + '_model2.h5')

    use_previous_weights1 = False
    previous_model_part1  = ''

    use_previous_weights2 = True
    previous_model_part2  = './Clark_et_al_2020_Spectral_Extrapolation/Duke_S_Extrap_Trained_Model.h5'

    use_numpy_weights1 = False
    use_numpy_weights2 = False

    npy_weights1 = ''
    npy_weights2 = ''


## Chain A and B masking

if True:

    mm_pix = (50*10)/768

    # Output size: batch_size,sh0[0],sh0[1],1
    B_mask_30 = make_diam_rmask( sh0[0], sh0[1], int(300/mm_pix), batch_size ) # 460, 30 of 33 cm for chain B
    B_mask_20 = make_diam_rmask( sh0[0], sh0[1], int(200/mm_pix), batch_size ) # 307, 20 of 33 cm for chain B

## Make a series of masks for measuring intensity isolevels

    # array([ 40, 80, 120, 160, 200, 240 ])
    # diam_rng = np.arange( 40, 240+20, 40 )
    diam_rng = np.arange( 40, 240+20, 20 )

    masks = np.zeros( (sh0[0], sh0[1], len(diam_rng) ) )

    for diam in diam_rng:

        masks[:,:,(diam-40)//20] = make_diam_rmask( sh0[0], sh0[1], int(diam/mm_pix), batch_size )[0,:,:,0]


## CNN Part 1: Map Chain A contrast to Chain B

if True:

# Inputs

    X_A_in_part1 = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='X_A_in_part1')
    # m30          = tf.convert_to_tensor( B_mask_30 )

# Map A to approximately match B

    X_A_ = X_A_in_part1

    X_A_ = tf.reshape( X_A_, [-1,1] )

    X_AB_out = PiecewiseLinearTransfer(grid=np.concatenate( [ [-1], np.linspace(0,2.8,15), [4] ], axis=0 ),min_val=-1, max_val=4, knots=17,name='PWL')( X_A_ )

    X_AB_out = tf.reshape( X_AB_out, [-1, sh0[0], sh0[1], 1] )

    X_AB_out_RMSE = keras.layers.Lambda(lambda x: x,name='X_AB_out_RMSE' )( X_AB_out )

# Compile / Optimize

    model1 = keras.models.Model(inputs=[X_A_in_part1], outputs=[X_AB_out_RMSE], name='model1')

    model1.summary(line_length=150)

    if use_previous_weights1 == True:

        # model1.load_weights(previous_model_part1,by_name=True)

        model1.load_weights( np.load(previous_model_part1) )

    if use_numpy_weights1 == True:

        weights = np.load( npy_weights1 )

        model1.set_weights( weights )


## CNN Part 2: Replace chain AB with chain B data within 20 cm mask, attempt to improve results further

if True:

    w  = fs//2

    def Conv_Block(L1,L2,D,max_pool=True):

        D  = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D  = L1(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]
        D  = LeakyReLU()( D )

        D  = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D  = L2(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]
        D_MP = LeakyReLU()( D )

        if max_pool:

            # D = MaxPool2D()( D_MP )

            sh = tf.shape( D )
            D = tf.image.resize_bilinear(D_MP,size=[sh[1]//2,sh[2]//2],align_corners=False,name=None,half_pixel_centers=False)

        else:

            D = D_MP

        return D_MP, D

    def ConvT_Block(D,L2,L1,D_,final_activation=True,skip_con=True):

        sh = tf.shape(D)

        D = tf.image.resize_bilinear(D,size=[sh[1]*2,sh[2]*2],align_corners=False,name=None,half_pixel_centers=False)

        # D = UpSampling2D()( D )

        if skip_con:

            D = tf.concat( [D,D_], axis=-1 )

        D = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D = L2(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]
        D = LeakyReLU()( D )

        D = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D = L1(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]

        if final_activation:

            D = LeakyReLU()( D )

        return D

# Attempt to improve the contrast further with a U-Net

    layer_names_part2 = [ 'L1_1', 'L1_2', 'L2_1', 'L2_2', 'L3_1', 'L3_2', 'L4_1', 'L4_2', 'L5_1', 'L5_2', 'L4_2t', 'L4_1t', 'L3_2t', 'L3_1t', 'L2_2t', 'L2_1t', 'L1_2t', 'L1_1t' ]

    # 768x768 => 384x384
    L1_1 = Conv2D(  filters= 16, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_1' )  # 8x
    L1_2 = Conv2D(  filters= 16, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_2' )

    # 384x384 => 192x192
    L2_1 = Conv2D(  filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_1' )  # 4x
    L2_2 = Conv2D(  filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_2' )

    # 192x192 => 96x96
    L3_1 = Conv2D(  filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_1' )  # 2x
    L3_2 = Conv2D(  filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_2' )

    # 96x96 => 48x48
    L4_1 = Conv2D(  filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_1' )  # 1x
    L4_2 = Conv2D(  filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_2' )

    # 48x48
    L5_1 = Conv2D(  filters=256, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L5_1' )  # 0.5x
    L5_2 = Conv2D(  filters=256, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L5_2' )

    # 96x96
    L4_2t = Conv2D( filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_2t' ) # 1x
    L4_1t = Conv2D( filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_1t' )

    # 192x192
    L3_2t = Conv2D( filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_2t' ) # 2x
    L3_1t = Conv2D( filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_1t' )

    # 384x384
    L2_2t = Conv2D( filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_2t' ) # 4x
    L2_1t = Conv2D( filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_1t' )

    # 768x768
    L1_2t = Conv2D( filters= 16, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_2t' ) # 8x
    L1_1t = Conv2D( filters=  2, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_1t' )

    wG = 15//2
    Gx,Gy = make_G_2D_sep( FWHM=7.0, w=15 )

    X0 = keras.layers.Input(shape=(sh0[0], sh0[1], 2), name='X0')

    C_LP1 = Conv2D( filters=1, kernel_size=(15,1), strides=(1,1), padding='SAME', name='C_LP1', use_bias=False, trainable=False )
    C_LP2 = Conv2D( filters=1, kernel_size=(1,15), strides=(1,1), padding='SAME', name='C_LP2', use_bias=False, trainable=False )

    X_Avg = tf.pad( X0, [[0,0],[wG,wG],[wG,wG],[0,0]], "REFLECT" )
    X_Avg = C_LP1( tf.reduce_mean(X_Avg,axis=-1,keepdims=True) )
    X_Avg = C_LP2( X_Avg )
    X_Avg = X_Avg[:,slice(wG,-wG),slice(wG,-wG),:]

    C_LP1.set_weights( [Gx] )
    C_LP2.set_weights( [Gy] )

    Xin = X0 - X_Avg

    if True:

        D = Xin

    # Block 1: 768x768 => 384x384

        D1,D = Conv_Block(L1_1,L1_2,D)

    # Block 2: 384x384 => 192x192

        D2,D = Conv_Block(L2_1,L2_2,D)

    # Block 3: 192x192 => 96x96

        D3,D = Conv_Block(L3_1,L3_2,D)

    # Block 4: 96x96 => 48x48

        D4,D = Conv_Block(L4_1,L4_2,D)

    # Block 5: 48x48

        _,D = Conv_Block(L5_1,L5_2,D,max_pool=False)

    # Block 4t: 48x48 => 96x96

        D = ConvT_Block(D,L4_2t,L4_1t,D4)

    # Block 3t: 96x96 => 192x192

        D = ConvT_Block(D,L3_2t,L3_1t,D3)

    # Block 2t: 192x192 => 384x384

        D = ConvT_Block(D,L2_2t,L2_1t,D2)

    # Block 1t: 384x384 => 768x768

        D = ConvT_Block(D,L1_2t,L1_1t,D1,final_activation=False)

    D += X_Avg

    base_model = keras.models.Model(inputs=[X0], outputs=[D], name='base_model')



# Network 2: Trainer

    if True:

        X_A_in = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='X_A_in')
        X_B_in = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='X_B_in')
        m20    = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='mask_20_in')
        m30    = tf.convert_to_tensor( B_mask_30 )

        M  = m20
        MB = m30-m20

        X_AB_out  = model1( X_A_in )
        X_AB_out2 = tf.stop_gradient(X_AB_out)

        # Substitute in real B
        X_B_out = fill_B(X_B_in,X_AB_out2,m20)

        # Don't use any real B
        # X_B_out = X_AB_out2

        # Use all real B
        # X_B_out = X_B_in

        X_out = base_model( tf.concat( [X_A_in,X_B_out], axis=-1 ) )

    # Losses

        X_AB_out      = keras.layers.Lambda(lambda x: x,name='XAB_out' ) ( X_AB_out )
        X_A_out_RMSE  = keras.layers.Lambda(lambda x: x,name='XA_RMSE' ) ( X_out[...,slice(0,1)] )
        X_B_out_RMSE  = keras.layers.Lambda(lambda x: x,name='XB_RMSE' ) ( X_out[...,slice(1,2)] )


    model2 = keras.models.Model(inputs=[X_A_in,X_B_in,m20], outputs=[ X_AB_out, X_A_out_RMSE, X_B_out_RMSE ], name='model2')

    model2.summary(line_length=150)

    if use_previous_weights2 == True:

        model2.load_weights(previous_model_part2,by_name=True)

    if use_numpy_weights2 == True:

        weights = np.load( npy_weights2 )
        model2.set_weights( weights )


'''
# Take measurements based on PowerPoint oval ROIs
'''

eval_gen  = list_gen_v4( bsize=batch_size, A_list_in=A_list_sub, B_list_in=B_list_sub,  case_list_in=case_list_sub, slice_th_list_in=th_list_sub, diam_list_in=diam_list_sub, \
                         sh0=sh0, target_slice_thickness=target_slice_thickness, target_diameter=target_diameter, current_pix_size=current_pix_size, \
                         mask_centers=mask_centers, mask_diams=mask_diams, mm_pix=mm_pix, B_mask_30=B_mask_30, \
                         random_order=False, augment=False, prior_histogram=False)

if True:

    prs = Presentation()
    blank_slide_layout = prs.slide_layouts[6]

    top   =  1
    left1 = -1
    left2 =  5

    tempA   = '/media/justify/DPC/Results_Temp/tempA.png'
    tempB   = '/media/justify/DPC/Results_Temp/tempB.png'
    tempC1  = '/media/justify/DPC/Results_Temp/tempC1.png'
    tempC2  = '/media/justify/DPC/Results_Temp/tempC2.png'
    ppt_out = '/media/justify/DPC/Results_Temp/ppt_out_masked.pptx'
    xls_out = '/media/justify/DPC/Results_Temp/ppt_out_masked.xlsx'

    # Create a workbook and add a worksheet.
    workbook  = xlsxwriter.Workbook(xls_out)
    worksheet = workbook.add_worksheet()
    crow      = 0

    for ep in range(0,eval_gen.__len__()):

        # The first slice = kidney_slice, so take the first sample of the batch
        bidx = 0

    # Apply network

        Xt, Yt = eval_gen.__getitem__(ep)

        XAB,XA,XB = model2.predict( Xt )

        X_A_in = Xt['X_A_in']
        X_B_in = Xt['X_B_in']
        mask20 = Xt['mask_20_in']

        C_in  = mat_decomp2(A=X_A_in[slice(0,1),:,:,slice(0,1)]*1000 - 1000,B=X_B_in[slice(0,1),:,:,slice(0,1)]*1000 - 1000)
        C_out = mat_decomp2(A=XA    [slice(0,1),:,:,slice(0,1)]*1000 - 1000,B=XB    [slice(0,1),:,:,slice(0,1)]*1000 - 1000)

    # Take measurements
    # assumes number of masks, etc. arranged as cases from the generator
    # Case Number - ROI Number - ROI Left (pix) - ROI Top (pix) - ROI Height (pix) - ROI Width (pix) - ROI Area (mm2) - Data Type - In Mask? - Min - Max - Mean - Standard Deviation

        ccase = case_list[ep][0]

        data_types = ['X_A_in'          ,'X_B_in'          ,'XA'          ,'XB'          ,'Iodine in'     ,'SoftTissue in'  ,'Iodine out'         ,'SoftTissue out']
        data_vals  = [X_A_in[bidx,:,:,0],X_B_in[bidx,:,:,0],XA[bidx,:,:,0],XB[bidx,:,:,0],C_in[bidx,:,:,0], C_in[bidx,:,:,1],C_out[bidx,:,:,0],C_out[bidx,:,:,1]]

        # unit conversion
        x_a        = [1000              ,1000              ,1000          ,1000          , 1              , 1               , 1               , 1]
        p_b        = [-1000             ,-1000             ,-1000         ,-1000         , 0              , 0               , 0               , 0]

        for i in range(0,len(rmasks[ep])): # ROIs

            N_els = float(np.sum( rmasks[ep][i] == 1 ))

            worksheet.write(crow, 0, ccase)                # Case Number
            worksheet.write(crow, 1, i)                    # ROI Number
            worksheet.write(crow, 2, rlefts[ep][i])        # ROI Left (pixels)
            worksheet.write(crow, 3, rtops[ep][i])         # ROI Top (pixels)
            worksheet.write(crow, 4, rheights[ep][i])      # ROI height (pixels)
            worksheet.write(crow, 5, rwidths[ep][i])       # ROI width (pixels)
            worksheet.write(crow, 6, N_els * mm_pix**2 )   # ROI Area (mm2)

            worksheet.write(crow, 7, mask_distance( mask=mask20[bidx,:,:,0], roi=rmasks[ep][i][bidx,:,:,0], sh0=sh0 ) ) # Distance from mask (pix), assumes circles

            dc = 8

            # add an extra column to separate measurement types
            dc += 1

            for vals,types,a,b in zip(data_vals,data_types,x_a,p_b): # Data types

                ROI_data   = vals[rmasks[ep][i][bidx,:,:,0] == 1]
                mask_check = float( np.any( np.logical_and( rmasks[ep][i][bidx,:,:,0] == 1, mask20[bidx,:,:,0] == 1 ) ) )

                ROI_data = ROI_data * a + b

                worksheet.write(crow,dc+0, types)                # Data Type
                worksheet.write(crow,dc+1, mask_check)           # In Mask?
                worksheet.write(crow,dc+2, np.amin( ROI_data ) ) # ROI min val
                worksheet.write(crow,dc+3, np.amax( ROI_data ) ) # ROI max val
                worksheet.write(crow,dc+4, np.mean( ROI_data ) ) # ROI mean val
                worksheet.write(crow,dc+5, np.std( ROI_data ) )  # ROI standard deviation

                dc += 6

                # add an extra column to separate measurement types
                dc += 1

            crow += 1

        # Add an extra row between cases
        crow += 1

    # Chain B mask size and shape (add position!)

        # Returns the current chain B mask diameter in mm
        # cdiam = eval_gen.get_diam()
        current_center, current_diam = eval_gen.get_diam()

        # mm => in
        img_size_in = 6
        rescale     = ( img_size_in / target_diameter )
        # offset      = ( (target_diameter - cdiam[bidx]) / 2.0 ) * rescale
        # circ_width  = cdiam[bidx] * rescale

        offsetx    = ( current_center[0] - current_diam / 2.0 ) * rescale
        offsety    = ( current_center[1] - current_diam / 2.0 ) * rescale
        circ_width = current_diam * rescale

    # Create slides with the ROIs drawn over an image

        XA_mod = np.copy( XA[bidx,:,:,0] )
        XB_mod = np.copy( XB[bidx,:,:,0] )

        XA_mod = np.tile( XA_mod[...,np.newaxis], [1,1,3] )
        XB_mod = np.tile( XB_mod[...,np.newaxis], [1,1,3] )

        for mod in range(0,3):

            for i in range(0,len(rmasks[ep])):

                XA_mod_ = XA_mod[...,mod]
                XB_mod_ = XB_mod[...,mod]

                if mod == 0:

                    mask__ = rmasks[ep][i][bidx,:,:,0] == 1
                    XA_mod_[ mask__ ] = 300
                    XB_mod_[ mask__ ] = 300

                else:

                    mask__ = rmasks[ep][i][bidx,:,:,0] == 1
                    XA_mod_[ mask__ ] = 0
                    XB_mod_[ mask__ ] = 0

                XA_mod[...,mod] = XA_mod_
                XB_mod[...,mod] = XB_mod_

        misc.toimage(XA_mod*1000 - 1000, cmin=-100.0, cmax=300).save(tempA)
        misc.toimage(XB_mod*1000 - 1000, cmin=-100.0, cmax=300).save(tempB)

        slide = prs.slides.add_slide(blank_slide_layout)

        txBox   = slide.shapes.add_textbox(left=Inches(-1), top=Inches(0), width=Inches(1), height=Inches(1))
        tf      = txBox.text_frame
        tf.text = "Case " + eval_gen.get_cases()[bidx] + ": X_A output, X_B output, [-100,300 HU]"

        pic1 = slide.shapes.add_picture(tempA, Inches(left1), Inches(top), height=Inches(img_size_in))
        pic2 = slide.shapes.add_picture(tempB, Inches(left2), Inches(top), height=Inches(img_size_in))

        shapes = slide.shapes
        shape  = shapes.add_shape( MSO_SHAPE.OVAL, left=Inches(left2+offsetx), top=Inches(top+offsety), width=Inches(circ_width), height=Inches(circ_width) )
        line   = shape.line
        line.color.rgb = RGBColor(255, 0, 0)
        line.width     = Pt(1.0)
        shape.fill.background()


# Save the Final result

    workbook.close()

    prs.save(ppt_out)