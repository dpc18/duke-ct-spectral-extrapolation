
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from tensorflow import keras

from tensorflow.python.keras.initializers import Constant

from tensorflow.python.keras.layers import Layer

import pydicom

import nibabel as nib

import numpy as np

from scipy import ndimage as nd
from scipy import interpolate


## Support classes and functions

if True:

# Custom piece-wise linear transfer function layer with learnable parameters

    class PiecewiseLinearTransfer(Layer):

        def __init__(self, grid, min_val=-1e6, max_val=1e6, knots=10, **kwargs):

            self.min_val = min_val
            self.max_val = max_val
            self.knots   = knots
            self.grid    = grid

            super().__init__(**kwargs)

        def build(self, input_shape):

            self.coeffs = self.add_weight(name='pw_linear_coeffs',
                                          shape=(self.knots,),
                                          initializer=Constant( self.grid ),
                                          trainable=True)

            super().build(input_shape)  # Be sure to call this at the end

        def call(self, X):

            # X   : b,1
            # grid: k
            # interval: b
            # interp: b,self.knots-1

            interval = tf.argmax( tf.cast( tf.logical_and( tf.greater( X, self.grid[0:self.knots-1] ),  tf.less( X, self.grid[1:self.knots] ) ), tf.float32), axis=-1 )

            a = ( X - self.grid[0:self.knots-1] ) / ( self.grid[1:self.knots] -  self.grid[0:self.knots-1] )
            b = ( self.grid[1:self.knots] - X ) / ( self.grid[1:self.knots] -  self.grid[0:self.knots-1] )

            interp = a * self.coeffs[1:self.knots] + b * self.coeffs[0:self.knots-1]

            interp   = tf.reshape(interp,[-1])
            interval = tf.cast( interval, tf.int32 ) + tf.range(0,tf.shape(X)[0]) * (self.knots-1)

            X_out = tf.gather( interp, interval )

            X_out = tf.reshape(X_out,tf.shape(X))

            return X_out

        def compute_output_shape(self, input_shape):

            return input_shape

# Gaussian smoothing kernel

    def make_G_2D_sep( FWHM=2.0, w=7 ):

        rad = int((w-1)/2)

        sigma = FWHM / ( 2 * np.sqrt(2 * np.log(2) ) )
        Gx = np.zeros((w,1,1,1),dtype=np.float32)
        Gy = np.zeros((1,w,1,1),dtype=np.float32)
        for i in range(-rad,rad+1):

            Gx[i+rad,...]   = np.exp( -( i**2 ) / ( 2 * ( sigma**2 ) ) )
            Gy[:,i+rad,...] = np.exp( -( i**2 ) / ( 2 * ( sigma**2 ) ) )

        Gx = Gx / np.sum(Gx)
        Gy = Gy / np.sum(Gy)

        return Gx, Gy


# Functions for reading in PowerPoint RoIs

    def get_coordinates( nx, ny, batch_size ):

        # rmask = np.zeros( (nx,ny), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0.0, 1.0 * nx ),
                              np.arange( 0.0, 1.0 * ny ), indexing='ij' )

        return x1, x2


    # widthROI = heightROI for circle, units of pixels relative to the top left of the image (not the slide)
    def get_circle_mask(x1,x2,topROI,leftROI,widthROI):

        x12 = x1 - topROI  - widthROI / 2.0
        x22 = x2 - leftROI - widthROI / 2.0

        rmask = np.zeros( x1.shape, dtype=np.float32 )

        rmask[ np.sqrt( np.add(x12**2, x22**2) ) <= widthROI / 2.0 ] = 1

        rmask = rmask[np.newaxis,:,:,np.newaxis]

        return rmask


    def get_oval_mask(x1,x2,topROI,leftROI,widthROI,heightROI,rotation):

    # Center coordinates on the center of the shape

        x12 = x1 - topROI  - heightROI / 2.0
        x22 = x2 - leftROI - widthROI  / 2.0

    # Rotate coordinates if needed

        if rotation != 0:

            sh0 = np.shape(x12)

            C = np.concatenate( (x22.flatten()[:,np.newaxis],x12.flatten()[:,np.newaxis]), axis=1 )

            rad = rotation * 2.0 * np.pi / 360.0

            C = C @ [[np.cos( rad ), -np.sin( rad )],[np.sin( rad ), np.cos( rad )]]

            x22 = np.reshape( C[:,0], sh0 )
            x12 = np.reshape( C[:,1], sh0 )

    # Compute mask

        rmask = np.zeros( x1.shape, dtype=np.float32 )

        # rmask[ np.sqrt( np.add(x12**2, x22**2) ) <= widthROI / 2.0 ] = 1

        if heightROI == widthROI:

            rmask[ np.sqrt( np.add(x12**2, x22**2) ) <= widthROI / 2.0 ] = 1

        else:

            rmask[ ( x12**2 / (heightROI/2.0)**2  + x22**2 / (widthROI/2.0)**2 ) <= 1.0 ] = 1

        rmask = rmask[np.newaxis,:,:,np.newaxis]

        return rmask


# Functions for chain A and B masks

    def fill_B(X_B,X_A_out,mask):

        X_B_out = (1-mask) * X_A_out + mask * X_B

        return X_B_out


    def make_diam_rmask00( nx, ny, px, py, diam, batch_size ):

        rmask = np.zeros( (nx,ny), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0.0, 1.0 * nx ),
                              np.arange( 0.0, 1.0 * ny ), indexing='ij' )

        x12 = x1 - py
        x22 = x2 - px

        rmask[ np.sqrt( np.add(x12**2, x22**2) ) <= diam/2.0 ] = 1

        rmask = rmask[np.newaxis,:,:,np.newaxis]

        rmask = np.tile( rmask, [batch_size,1,1,1] )

        return rmask


    def make_diam_rmask0( nx, ny, px, py, pbx, pby, diam ):

        rmask = np.zeros( (nx,ny), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0.0, 1.0 * nx ),
                              np.arange( 0.0, 1.0 * ny ), indexing='ij' )

        x12 = x1 - py
        x22 = x2 - px

        rmask[ np.sqrt( np.add(x12**2, x22**2) ) <= diam/2.0 ] = 1

        for i in range(0, len(pbx)):

            xl = pbx[i][0]
            xh = pbx[i][1]

            yl = pby[i][0]
            yh = pby[i][1]

            rmask[ np.logical_and( np.logical_and( np.logical_and( x1 >= yl, x1 <= yh), x2 >= xl ), x2 <= xh) ] = 2

        return rmask


    def make_diam_rmask( nx, ny, diam, batch_size ):

        rmask = np.zeros( (nx,ny), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1 ),
                              np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1), indexing='ij' )

        rmask[ np.sqrt( np.add(x1**2, x2**2) ) <= diam/2.0 ] = 1

        rmask = rmask[np.newaxis,:,:,np.newaxis]

        rmask = np.tile( rmask, [batch_size,1,1,1] )

        return rmask


    def mask_distance( mask, roi, sh0 ):

        def COM(x,y,mask):

            mx = np.sum( x*mask ) / np.sum(mask)
            my = np.sum( y*mask ) / np.sum(mask)

            return mx, my

        rmask = np.zeros( (sh0[0],sh0[1]), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0.0, 1.0 * sh0[0] ),
                              np.arange( 0.0, 1.0 * sh0[1] ), indexing='ij' )

        mx,my = COM( x1,x2,mask )
        rx,ry = COM( x1,x2,roi )

        c = np.sqrt( (mx-rx)**2 + (my-ry)**2 )

        rMask = np.sqrt( np.sum( mask ) / np.pi )
        rRoi  = np.sqrt( np.sum( roi ) / np.pi )

        dist = c - rMask - rRoi

        return dist


# Material decomposition

    def mat_decomp2(A,B):
    # Input data files are assumed to be correctly scaled in HU.

        def pinv_vec(x):

            u, s, vt = np.linalg.svd(x,full_matrices=False)

            v = np.transpose(vt,[1,0])

            # return ( v * (1/s) ) @ tf.transpose(u,[1,0])

            return ( v * (1/np.maximum(s,1e-3)) ) @ np.transpose(u,[1,0])

        def pinv_mat(x):

            u, s, vt = np.linalg.svd(x,full_matrices=False)

            v = np.transpose(vt,[1,0])

            # cond = s[1] / s[0]

            # s = tf.Print(s,[s,cond],'Sensitivity Matrix (SVs, cond #): ')

            return [( v @ np.diag(1/np.maximum(s,1e-3)) ) @ np.transpose(u,[1,0]), s]

        def apply_LS_decomp(X, M):

            vals   = X

            Mt, S  = pinv_mat( M )

            C      = vals @ Mt

            return [C, S]

        A = A.astype('float64')
        B = B.astype('float64')

        # iodine, water
        M1 = np.array([[1.2432020, 0.4287106],
                       [0.9973984, 1.0179663]])

        sh = A.shape
        X = np.concatenate( ( np.reshape(A,[-1,1]), np.reshape(B,[-1,1]) ), axis=1 )

        C1,_ = apply_LS_decomp(X,M1)

        C1 = C1.astype('float32')

        C1 = np.reshape( C1, [sh[0],sh[1],sh[2],2] )

        return C1


# Data generator

    class list_gen_v4(keras.utils.Sequence):

        # Handle variable input slice thickness (1.5 mm or 3.0 mm)
        # Does not account for how much the slices may overlap...!

        # Use mask_centers and mask_diams parameters to avoid kidney lesions
        # Return the mask parameters for drawing circles

        def __init__(self, bsize, A_list_in, B_list_in, case_list_in, slice_th_list_in, diam_list_in, \
                     sh0, target_slice_thickness, target_diameter, current_pix_size, mask_centers, mask_diams, mm_pix, B_mask_30, \
                     random_order=False, augment=False, override_mask=False, omask=None, nifti_source=None, prior_histogram=False):

            self.prior_histogram = prior_histogram

            if self.prior_histogram:

                ref_hist_file = '/media/x-ray/blkbeauty4/Duke_S_Extrap_2019_Results/Ref_histogram.npy'
                _,self.valid_combos_A,self.valid_combos_B = np.load( ref_hist_file )

            self.bsize = bsize

            # self.avg_slices = avg_slices
            self.slice_th_list_in = slice_th_list_in
            self.diam_list_in     = diam_list_in

            self.A_list_f      = []     # all chain A files in one list
            self.B_list_f      = []     # all chain B files in one list
            self.case_list     = []     # string 'Case #' for all files in the A and B lists
            self.current_cases = [] # list of cases associated with the current batch; requires serial execution!

            self.slice_th_list = []
            self.diams         = []
            self.slices_per_case = {}

            self.sh0 = sh0

            self.target_slice_thickness = target_slice_thickness

            self.current_pix_size = current_pix_size
            self.mask_centers = mask_centers
            self.mask_diams   = mask_diams
            self.B_mask_30    = B_mask_30
            self.mm_pix       = mm_pix

            # Count the total number of batches
            self.total_batches = 0

        # Report the current diameter for drawing circles

            # self.current_diam = None

            self.current_mask_center = None
            self.current_mask_diam   = None

        # Precomputation for interpolation

            self.pix_size   = target_diameter / self.sh0[0]
            self.target_pix = np.arange( self.pix_size * ( -(self.sh0[0]//2)+0.5 ), self.pix_size * ( (self.sh0[0]//2)-0.499999 ), self.pix_size )

            self.xx2, self.yy2 = self.target_pix, self.target_pix

        # Process the data with a static ~20 cm mask

            self.override_mask = override_mask
            self.omask = omask

            self.nifti_source = nifti_source

            if self.nifti_source is not None:

                self.data = nib.load(nifti_source).get_data().astype('float32')

                # make sure the data shape is divisible by the batch size, so all data will be processed
                assert self.bsize * ( self.data.shape[-1] // self.bsize ) == self.data.shape[-1]

                self.total_batches = self.data.shape[-1] // self.bsize

            else:

            ## Skip slices so we stay divisible by the number of averaged slices, avoid averaging mis-matched slices
            ## Note: Only full batches are processed.
            # Reformat from a [case,slices] array to a [batch,bsize,slices] array to handle variable slice thickness implicitly.

                b = 0
                for i in range(0,len(A_list_in)): # cases

                    # Assumes all slices in the same case have the same slice thickness
                    avg_slices = int( self.target_slice_thickness // self.slice_th_list_in[i][0] )

                    sz = ( len(A_list_in[i]) // avg_slices ) * avg_slices

                    # Rounds down to avoid incomplete batches
                    num_batches = int( sz // ( self.bsize * avg_slices ) )

                    self.slices_per_case[ case_list_in[i][0] ] = num_batches * self.bsize

                    c = 0
                    for j in range(0,num_batches): # batches

                        self.A_list_f.append     ( [] )
                        self.B_list_f.append     ( [] )
                        self.case_list.append    ( [] )
                        self.slice_th_list.append( [] )
                        self.diams.append        ( [] )

                        for k in range(0,self.bsize): # averaged slices / batch

                            self.A_list_f     [b].append( A_list_in            [i][slice(c,c+avg_slices)] )
                            self.B_list_f     [b].append( B_list_in            [i][slice(c,c+avg_slices)] )

                            self.case_list    [b].append( case_list_in         [i][slice(c,c+avg_slices)] )

                            self.slice_th_list[b].append( self.slice_th_list_in[i][slice(c,c+avg_slices)] )
                            self.diams        [b].append( self.diam_list_in    [i][slice(c,c+avg_slices)] )

                            c += avg_slices

                        b += 1

                self.total_batches = b

        ## Go through all slices in order, or randomize the order?
        ## Only average consecutive slices.

            self.random_order = random_order
            self.augment      = augment

            if self.random_order:

                self.order = np.random.permutation( self.total_batches )

            else:

                self.order = np.arange(0,self.total_batches)


        def __len__(self):

            'Denotes the number of batches per epoch'
            return self.total_batches


        def change_override_mask(self,override_mask=False, omask=None):

            self.override_mask = override_mask

            self.omask = omask


        def get_cases(self):

            return self.current_cases


        def randomize_order(self):

            self.order = np.random.permutation( self.total_batches )


        def reverse_interp(self, XA, XB, idx):

            index = self.order[idx]

            out_img_A   = np.zeros( (self.bsize,self.current_pix_size,self.current_pix_size,1), dtype=np.float32 )
            out_img_B   = np.zeros( (self.bsize,self.current_pix_size,self.current_pix_size,1), dtype=np.float32 )

        ## Return data to its original scale

            XA = XA * 1000 - 1000
            XB = XB * 1000 - 1000

        ## Return data to the original format

            if self.nifti_source is not None:

                print('Nifti source not implemented for generator...!')

                return

            else:

                # Exploit the fact that cases are assigned to complete batches to do precomputation
                diam        = self.diams[index][0][0]
                pix_size    = diam / self.current_pix_size
                current_pix = np.arange( pix_size*( -(self.current_pix_size//2)+0.5 ), pix_size * ( (self.current_pix_size//2)-0.499999 ), pix_size )
                xx , yy     = current_pix, current_pix

                for b in range(0,self.bsize): # batch

                # Reverse interpolation

                    if self.pix_size != pix_size:

                        interp_fun  = interpolate.interp2d( self.xx2, self.yy2, XA[slice(b,b+1),...], fill_value=-1000 )
                        out_img_A_  = interp_fun( xx, yy )

                        interp_fun  = interpolate.interp2d( self.xx2, self.yy2, XB[slice(b,b+1),...], fill_value=-1000 )
                        out_img_B_  = interp_fun( xx, yy )

                    else:

                        cx = (self.sh0[0] - self.current_pix_size)//2
                        cy = (self.sh0[1] - self.current_pix_size)//2

                        out_img_A_ = XA[slice(b,b+1),slice(cx,-cx),slice(cy,-cy),:]
                        out_img_B_ = XB[slice(b,b+1),slice(cx,-cx),slice(cy,-cy),:]

                    out_img_A[b,...,0] = out_img_A_
                    out_img_B[b,...,0] = out_img_B_

        ## Mask chain B data and rescale to positive units
        ## Exploit the fact that cases are assigned to complete batches to process whole batches with the same parameters

                b = 0
                s = 0 # even if multiple slices were averaged, we only need to read one DICOM

                temp        = pydicom.dcmread(self.A_list_f[index][b][s])
                out_img_A   = ( out_img_A - float( temp.RescaleIntercept ) ) / float(temp.RescaleSlope)

                 # Set data exactly to zero outside the chain A FoV, as this is the original format
                mask       = temp.pixel_array != 0
                out_img_A *= mask[np.newaxis,:,:,np.newaxis]

                temp        = pydicom.dcmread(self.B_list_f[index][b][s])
                out_img_B   = ( out_img_B - float( temp.RescaleIntercept ) ) / float(temp.RescaleSlope)

                # Spectral processing requires 0 outside of the nominal chain B FoV
                mask       = temp.pixel_array != 0
                out_img_B *= mask[np.newaxis,:,:,np.newaxis]

                # Make sure there are no remaining negative values
                out_img_A[ out_img_A < 0 ] = 0
                out_img_B[ out_img_B < 0 ] = 0

            return out_img_A, out_img_B


        def __getitem__(self, idx):

            index = self.order[idx]

            while True:

                # load and average
                out_img_A   = np.zeros( (self.bsize,self.sh0[0],self.sh0[1],1), dtype=np.float32 )
                out_img_B   = np.zeros( (self.bsize,self.sh0[0],self.sh0[1],1), dtype=np.float32 )

            ## Choose data

                self.current_cases = []

                if self.nifti_source is not None:

                    print('Nifti source not implemented for generator...!')

                    return

                else:

                    # Exploit the fact that cases are assigned to complete batches to do precomputation
                    diam        = self.diams[index][0][0]
                    pix_size    = diam / self.current_pix_size
                    current_pix = np.arange( pix_size*( -(self.current_pix_size//2)+0.5 ), pix_size * ( (self.current_pix_size//2)-0.499999 ), pix_size )
                    xx , yy     = current_pix, current_pix

                    for b in range(0,self.bsize): # batch

                        avg_slices = len(self.A_list_f[index][b])

                        out_img_A_ = 0
                        out_img_B_ = 0

                        self.current_cases.append( self.case_list[index][b][0] )

                        for s in range(0,avg_slices): # slice

                            temp        = pydicom.dcmread(self.A_list_f[index][b][s])
                            img         = temp.pixel_array
                            out_img_A_ += ( img.astype('float32') * float(temp.RescaleSlope) + float( temp.RescaleIntercept ) )

                            temp        = pydicom.dcmread(self.B_list_f[index][b][s])
                            img         = temp.pixel_array
                            out_img_B_ += ( img.astype('float32') * float(temp.RescaleSlope) + float( temp.RescaleIntercept ) )

                        out_img_A_ /= avg_slices
                        out_img_B_ /= avg_slices

                        out_img_A_[ out_img_A_ == float( temp.RescaleIntercept ) ] = -1000
                        out_img_B_[ out_img_B_ == float( temp.RescaleIntercept ) ] = -1000

                        if self.pix_size != pix_size:

                            interp_fun  = interpolate.interp2d( xx, yy, out_img_A_, fill_value=-1000 )
                            out_img_A_  = interp_fun( self.xx2, self.yy2 )

                            interp_fun  = interpolate.interp2d( xx, yy, out_img_B_, fill_value=-1000 )
                            out_img_B_  = interp_fun( self.xx2, self.yy2 )

                            out_img_A[b,...,0] = out_img_A_
                            out_img_B[b,...,0] = out_img_B_

                        else:

                            cx = (self.sh0[0] - self.current_pix_size)//2
                            cy = (self.sh0[1] - self.current_pix_size)//2

                            out_img_A[b,slice(cx,-cx),slice(cy,-cy),0] = out_img_A_
                            out_img_B[b,slice(cx,-cx),slice(cy,-cy),0] = out_img_B_



            ## Apply flips

                if self.augment:

                    doflip = np.random.randint(4)

                    # assumes the masks are circularly symmetric and do not need to be flipped
                    if doflip == 1:

                        out_img_A  = np.flip( out_img_A, axis=2 )
                        out_img_B  = np.flip( out_img_B, axis=2 )

                    elif doflip == 2:

                        out_img_A  = np.flip( out_img_A, axis=1 )
                        out_img_B  = np.flip( out_img_B, axis=1 )

                    elif doflip == 3:

                        out_img_A  = np.flip( out_img_A, axis=1 )
                        out_img_B  = np.flip( out_img_B, axis=1 )

                        out_img_A  = np.flip( out_img_A, axis=2 )
                        out_img_B  = np.flip( out_img_B, axis=2 )

            ## Rotate

                if self.augment:

                    theta1 = 180*(np.random.rand()-0.5)/0.5

                    out_img_A = nd.rotate(out_img_A, angle=theta1, axes=[1,2], reshape=False, cval=-1000)
                    out_img_B = nd.rotate(out_img_B, angle=theta1, axes=[1,2], reshape=False, cval=-1000)

                    # out_img_A /= 1000
                    # out_img_B /= 1000

            ## Intensity scaling

                # air => 0, water => 1
                out_img_A = (out_img_A + 1000)/1000
                out_img_B = (out_img_B + 1000)/1000

            ## Create the correct mask

                # Exploit the fact that batches are assigned to complete cases
                ccase = self.case_list[index][0][0]
                self.current_mask_center = self.mask_centers[ccase]
                self.current_mask_diam   = self.mask_diams[ccase]
                px_   = self.mask_centers[ccase][0] / self.mm_pix
                py_   = self.mask_centers[ccase][1] / self.mm_pix
                diam_ = self.mask_diams[ccase]      / self.mm_pix

                mask_20_out = make_diam_rmask00( nx=self.sh0[0], ny=self.sh0[1], px=px_, py=py_, diam=diam_, batch_size=self.bsize )

                # Stay within the 30 cm FoV of chain B
                mask_20_out *= self.B_mask_30

            ## If we are using a prior histogram, replace the first sample

                if self.prior_histogram:

                    idx = np.random.randint( low=0, high=self.valid_combos_A.shape[0] )

                    IA = self.valid_combos_A[idx]
                    IB = self.valid_combos_B[idx]

                    out_img_A[slice(0,1),...] = out_img_A[slice(0,1),...] * 0.0 + IA + 0.03 * np.random.normal( size=(1,self.sh0[0],self.sh0[1],1) )
                    out_img_B[slice(0,1),...] = out_img_B[slice(0,1),...] * 0.0 + IB + 0.03 * np.random.normal( size=(1,self.sh0[0],self.sh0[1],1) )

            ## Output

                out_zeros = np.zeros( (self.bsize,), dtype=np.float32 )

                return {'X_A_in':out_img_A,'X_B_in':out_img_B,'mask_20_in':mask_20_out}, \
                       {'XAB_out':out_img_B, 'XA_RMSE':out_img_A, 'XB_RMSE':out_img_B }


        def get_diam(self):

            # Returns the current diameter in mm

            # return self.current_diam

            return self.current_mask_center, self.current_mask_diam

