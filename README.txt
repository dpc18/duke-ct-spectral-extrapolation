This repository includes source code to support the following two publications dealing with
deep-learning based extension of clinical, dual-source x-ray CT data for full field of view,
dual-energy post-processing when one detector covers a smaller field of view. We refer to
this process as spectral extrapolation.

(1) Clark, D. P., Schwartz, F. R., Marin, D., Ramirez‐Giraldo, J. C., & Badea, C. T. (2020).
    Deep learning based spectral extrapolation for dual‐source, dual‐energy x‐ray computed
    tomography. Medical Physics, 47(9), 4150-4163.

(2) Schwartz, F. R., Clark, D. P., Ding, Y., Ramirez-Giraldo, J. C., Badea, C. T., & Marin,
    D. (2021). Evaluating renal lesions using deep-learning based extension of dual-energy
    FoV in dual-source CT – a retrospective pilot study. European Journal of Radiology,
    109734.

The “Clark_et_al_2020_Spectral_Extrapolation” subdirectory includes source code, support
functions, and a saved, trained model used to produce the spectral extrapolation results
presented in publication (1).

The “Regina_Schwartz_et_al_2021_Renal_Lesions” subdirectory includes source code used to
place masks on kidney lesion data sets to assess performance in extrapolating lesion
enhancement (publication 2).

Regrettably, due to institutional and legal restrictions we are unable to share the
clinical patient data on which our model has been trained and evaluated. This includes
the PowerPoint files referenced in the Duke_S_Extrap_Apply_Trained_Model.py file.

