
# Train the spectral extrapolation model using pairs of chain A and Chain B data.

# Imports
if True:

    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()

    from tensorflow import keras

    import nibabel as nib
    from datetime import datetime

    import numpy as np

    from functools import partial

    from tensorflow.python.keras.layers import Conv2D, Activation, UpSampling2D, LeakyReLU

    np.random.seed(seed=317)

    import glob

    import os

    from Clark_et_al_2020_Spectral_Extrapolation.Duke_S_Extrap_Train_Support import AdamAccumulate_L4, list_gen, PiecewiseLinearTransfer, make_diam_rmask, fill_B, RMSE, masked_RMSE, \
                                                                                    make_G_2D_1D


## Network parameters

if True:

## Data sets

    data_base = '/media/x-ray/bbarchive/Duke_S_Extrap_2019/Patient_Data_7_3_19/'

    sub_dirs = ['EDCT1_7_24_19','RAW_DATA_CC_CT4','RAW_DATA_CC_CT5','RAW_DATA_CTC1','RAW_DATA_CTC3']

    # NOTE: [] are reserved characters in glob expressions
    data_A = '*_DE/*_[A*/*.dcm'
    data_B = '*_DE/*_[B*/*.dcm'

    A_list = []
    B_list = []

    total_slices = 0

    all_cases = []

    case_list = []

    for l in range(0,len(sub_dirs)):

        rpath = data_base + sub_dirs[l]

        all_cases += os.listdir(rpath)

        current_cases = os.listdir(rpath)

        cases = glob.glob( rpath + '/*/' )

        for j in range(0,len(cases)):

            rpathA = cases[j] + data_A
            rpathB = cases[j] + data_B

            caseA = glob.glob( rpathA )
            caseB = glob.glob( rpathB )

            cA = sorted(caseA)

            A_list.append( cA )
            B_list.append( sorted(caseB) )

            case_list.append( [ current_cases[j] ] * len(cA) )

            total_slices += len(caseA)

    print('Total slices (0.75 mm): ' + str(total_slices))

## Pull out validation and test data

    # EDCT1_7_24_19:   0-12    (200s)
    # RAW_DATA_CC_CT4: 13-19   (300s)
    # RAW_DATA_CC_CT5: 20-31   (400s)
    # RAW_DATA_CTC1:   32-41   (001s)
    # RAW_DATA_CTC3:   42-50   (100s)

    val_list   = [ 0,13,20,32,42]
    test_list  = [ 1,14,21,33,43]

    A_list_val    = [ A_list[i] for i in val_list ]
    B_list_val    = [ B_list[i] for i in val_list ]
    val_cases     = [ all_cases[i] for i in val_list ]
    val_case_list = [ case_list[i] for i in val_list ]

    A_list_test    = [ A_list[i] for i in test_list ]
    B_list_test    = [ B_list[i] for i in test_list ]
    test_cases     = [ all_cases[i] for i in test_list ]
    test_case_list = [ case_list[i] for i in test_list ]

    A_list          = [ A_list[i]    for i in range(0,len(A_list)) if i not in val_list + test_list ]
    B_list          = [ B_list[i]    for i in range(0,len(B_list)) if i not in val_list + test_list ]
    train_cases     = [ all_cases[i] for i in range(0,len(all_cases)) if i not in val_list + test_list ]
    train_case_list = [ case_list[i] for i in range(0,len(case_list)) if i not in val_list + test_list ]

## Parameters

    # These should be divisible for feed through
    batch_size  =  4 # 8 # 16
    avg_slices  =  4 # number of 0.75 mm slices to average

    accum_iters =   1 # 1 # 8  # 16
    epochs      = 100 # 100
    do_train    = True # False

    decay_factor = 10  # decay by one order of magnitude over training
    # decay_factor = 100  # decay by one order of magnitude over training

    fs = 3 # 4 # 5

    sh0 = [768,768]

    dbase   = '/media/x-ray/blkbeauty4/Convolutional_denoising/CNN_denoising/'

    outpath_tf   = '/media/x-ray/blkbeauty4/Duke_S_Extrap_2019_Results/TF_Metrics/'

    outpath_base = '/media/x-ray/blkbeauty4/Duke_S_Extrap_2019_Results/Outputs/'

    run_name     = 'Duke_S_Extrap_2019_v23'

    path = os.path.join(outpath_base + run_name + '_{}'.format(datetime.strftime(datetime.now(), '%Y_%m_%d_%H-%M-%S')))

    if not os.path.isdir(path):
        os.mkdir(path)

    model1_name    = os.path.join(path, run_name + '_model1.h5')
    model2_name    = os.path.join(path, run_name + '_model2.h5')

    use_previous_weights1 = False
    previous_model_part1  = ''

    use_previous_weights2 = True
    previous_model_part2  = './Clark_et_al_2020_Spectral_Extrapolation/Duke_S_Extrap_Trained_Model.h5'


    use_numpy_weights1 = False
    use_numpy_weights2 = False

    npy_weights1 = ''
    npy_weights2 = ''


## Data Masks

if True:

    mm_pix = (50*10)/768

    # Output size: batch_size,sh0[0],sh0[1],1
    B_mask_30 = make_diam_rmask( sh0[0], sh0[1], int(300/mm_pix), batch_size ) # 460, 30 of 33 cm for chain B
    B_mask_20 = make_diam_rmask( sh0[0], sh0[1], int(200/mm_pix), batch_size ) # 307, 20 of 33 cm for chain B

## Make a series of masks for measuring intensity isolevels

    # array([ 40, 80, 120, 160, 200, 240 ])
    # diam_rng = np.arange( 40, 240+20, 40 )
    diam_rng = np.arange( 40, 240+20, 20 )

    masks = np.zeros( (sh0[0], sh0[1], len(diam_rng) ) )

    for diam in diam_rng:

        masks[:,:,(diam-40)//20] = make_diam_rmask( sh0[0], sh0[1], int(diam/mm_pix), batch_size )[0,:,:,0]


## CNN Part 1: Map Chain A contrast to Chain B

if True:

# Inputs

    X_A_in_part1 = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='X_A_in_part1')
    # m30          = tf.convert_to_tensor( B_mask_30 )

# Map A to approximately match B

    X_A_ = X_A_in_part1

    X_A_ = tf.reshape( X_A_, [-1,1] )

    X_AB_out = PiecewiseLinearTransfer(grid=np.concatenate( [ [-1], np.linspace(0,2.8,15), [4] ], axis=0 ),min_val=-1, max_val=4, knots=17, name='PWL')( X_A_ )

    X_AB_out = tf.reshape( X_AB_out, [-1, sh0[0], sh0[1], 1] )

    X_AB_out_RMSE = keras.layers.Lambda(lambda x: x,name='X_AB_out_RMSE' )( X_AB_out )

# Compile / Optimize

    model1 = keras.models.Model(inputs=[X_A_in_part1], outputs=[X_AB_out_RMSE], name='model1')

    model1.summary(line_length=150)

    if use_previous_weights1 == True:

        # model1.load_weights(previous_model_part1,by_name=True)

        model1.load_weights( np.load(previous_model_part1) )

    if use_numpy_weights1 == True:

        weights = np.load( npy_weights1 )

        model1.set_weights( weights )


## CNN Part 2: Replace chain AB with chain B data within 20 cm mask, attempt to improve results further

if True:

    w  = fs//2

    def Conv_Block(L1,L2,D,max_pool=True):

        D  = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D  = L1(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]
        D  = LeakyReLU()( D )

        D  = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D  = L2(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]
        D_MP = LeakyReLU()( D )

        if max_pool:

            # D = MaxPool2D()( D_MP )

            sh = tf.shape( D )
            D = tf.image.resize_bilinear(D_MP,size=[sh[1]//2,sh[2]//2],align_corners=False,name=None,half_pixel_centers=False)

        else:

            D = D_MP

        return D_MP, D

    def ConvT_Block(D,L2,L1,D_,final_activation=True,skip_con=True):

        sh = tf.shape(D)

        D = tf.image.resize_bilinear(D,size=[sh[1]*2,sh[2]*2],align_corners=False,name=None,half_pixel_centers=False)

        # D = UpSampling2D()( D )

        if skip_con:

            D = tf.concat( [D,D_], axis=-1 )

        D = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D = L2(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]
        D = LeakyReLU()( D )

        D = tf.pad( D, [[0,0],[w,w],[w,w],[0,0]], "REFLECT" )
        D = L1(D)
        D  = D[:,slice(w,-w),slice(w,-w),:]

        if final_activation:

            D = LeakyReLU()( D )

        return D

# Attempt to improve the contrast further with a U-Net

    layer_names_part2 = [ 'L1_1', 'L1_2', 'L2_1', 'L2_2', 'L3_1', 'L3_2', 'L4_1', 'L4_2', 'L5_1', 'L5_2', 'L4_2t', 'L4_1t', 'L3_2t', 'L3_1t', 'L2_2t', 'L2_1t', 'L1_2t', 'L1_1t' ]

    # 768x768 => 384x384
    L1_1 = Conv2D(  filters= 16, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_1' )  # 8x
    L1_2 = Conv2D(  filters= 16, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_2' )

    # 384x384 => 192x192
    L2_1 = Conv2D(  filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_1' )  # 4x
    L2_2 = Conv2D(  filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_2' )

    # 192x192 => 96x96
    L3_1 = Conv2D(  filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_1' )  # 2x
    L3_2 = Conv2D(  filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_2' )

    # 96x96 => 48x48
    L4_1 = Conv2D(  filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_1' )  # 1x
    L4_2 = Conv2D(  filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_2' )

    # 48x48
    L5_1 = Conv2D(  filters=256, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L5_1' )  # 0.5x
    L5_2 = Conv2D(  filters=256, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L5_2' )

    # 96x96
    L4_2t = Conv2D( filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_2t' ) # 1x
    L4_1t = Conv2D( filters=128, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L4_1t' )

    # 192x192
    L3_2t = Conv2D( filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_2t' ) # 2x
    L3_1t = Conv2D( filters= 64, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L3_1t' )

    # 384x384
    L2_2t = Conv2D( filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_2t' ) # 4x
    L2_1t = Conv2D( filters= 32, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L2_1t' )

    # 768x768
    L1_2t = Conv2D( filters= 16, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_2t' ) # 8x
    L1_1t = Conv2D( filters=  2, kernel_size=(fs,fs), strides=(1,1), padding='SAME', name='L1_1t' )



    wG = 15//2
    Gx,Gy = make_G_2D_1D( FWHM=7.0, w=15 )

    X0 = keras.layers.Input(shape=(sh0[0], sh0[1], 2), name='X0')

    C_LP1 = Conv2D( filters=1, kernel_size=(15,1), strides=(1,1), padding='SAME', name='C_LP1', use_bias=False, trainable=False )
    C_LP2 = Conv2D( filters=1, kernel_size=(1,15), strides=(1,1), padding='SAME', name='C_LP2', use_bias=False, trainable=False )

    X_Avg = tf.pad( X0, [[0,0],[wG,wG],[wG,wG],[0,0]], "REFLECT" )
    X_Avg = C_LP1( tf.reduce_mean(X_Avg,axis=-1,keepdims=True) )
    X_Avg = C_LP2( X_Avg )
    X_Avg = X_Avg[:,slice(wG,-wG),slice(wG,-wG),:]

    C_LP1.set_weights( [Gx] )
    C_LP2.set_weights( [Gy] )

    Xin = X0 - X_Avg

    if True:

        D = Xin

    # Block 1: 768x768 => 384x384

        D1,D = Conv_Block(L1_1,L1_2,D)

    # Block 2: 384x384 => 192x192

        D2,D = Conv_Block(L2_1,L2_2,D)

    # Block 3: 192x192 => 96x96

        D3,D = Conv_Block(L3_1,L3_2,D)

    # Block 4: 96x96 => 48x48

        D4,D = Conv_Block(L4_1,L4_2,D)

    # Block 5: 48x48

        _,D = Conv_Block(L5_1,L5_2,D,max_pool=False)

    # Block 4t: 48x48 => 96x96

        D = ConvT_Block(D,L4_2t,L4_1t,D4)

    # Block 3t: 96x96 => 192x192

        D = ConvT_Block(D,L3_2t,L3_1t,D3)

    # Block 2t: 192x192 => 384x384

        D = ConvT_Block(D,L2_2t,L2_1t,D2)

    # Block 1t: 384x384 => 768x768

        D = ConvT_Block(D,L1_2t,L1_1t,D1,final_activation=False)

    D += X_Avg

    base_model = keras.models.Model(inputs=[X0], outputs=[D], name='base_model')



# Setup for network trianing...

    if True:

        X_A_in = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='X_A_in')
        X_B_in = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='X_B_in')
        m20    = keras.layers.Input(shape=(sh0[0], sh0[1], 1), name='mask_20_in')
        m30    = tf.convert_to_tensor( B_mask_30 )

        M  = m20
        MB = m30-m20

        X_AB_out  = model1( X_A_in )
        X_AB_out2 = tf.stop_gradient(X_AB_out)

        X_B_out = fill_B(X_B_in,X_AB_out2,m20)

        X_out = base_model( tf.concat( [X_A_in,X_B_out], axis=-1 ) )

    # Losses

        X_AB_out      = keras.layers.Lambda(lambda x: x,name='XAB_out' ) ( X_AB_out )
        X_A_out_RMSE  = keras.layers.Lambda(lambda x: x,name='XA_RMSE' ) ( X_out[...,slice(0,1)] )
        X_B_out_RMSE  = keras.layers.Lambda(lambda x: x,name='XB_RMSE' ) ( X_out[...,slice(1,2)] )


    RMSE_30_mask          = partial(masked_RMSE, mask=m30)
    RMSE_30_mask.__name__ = 'RMSE_30_mask'

    # RMSE_20_mask          = partial(masked_RMSE, mask=m30-m20)
    # RMSE_20_mask.__name__ = 'RMSE_20_mask'

    model2 = keras.models.Model(inputs=[X_A_in,X_B_in,m20], outputs=[ X_AB_out, X_A_out_RMSE, X_B_out_RMSE ], name='model2')

    model2.summary(line_length=150)

    if use_previous_weights2 == True:

        model2.load_weights(previous_model_part2,by_name=True)

    if use_numpy_weights2 == True:

        weights = np.load( npy_weights2 )
        model2.set_weights( weights )

    trainer_gen  = list_gen( bsize=batch_size, A_list_in=A_list, B_list_in=B_list,  case_list_in=train_case_list, avg_slices=avg_slices, \
                             sh0=sh0, masks=masks, mm_pix=mm_pix, diam_rng=diam_rng, random_order=True, augment=True, prior_histogram=False)

    decay = ( decay_factor - 1 ) / ( epochs * trainer_gen.__len__() )

    opt = AdamAccumulate_L4(lr=3e-4, accum_iters=accum_iters, decay=decay, L2_decay=0.3 )

    model2.compile(optimizer=opt, loss={ 'XAB_out':RMSE_30_mask, 'XA_RMSE':RMSE, 'XB_RMSE':RMSE_30_mask },
                   loss_weights=[ 1.0, 1.0, 1.0 ])


## Training

if True:

    tboard = keras.callbacks.TensorBoard(log_dir=outpath_tf + 'tflog_' + run_name + '_{}'.format(datetime.strftime(datetime.now(), '%Y_%m_%d_%H-%M-%S')),
                                         write_graph=False,
                                         write_grads=False,
                                         write_images=False,
                                         histogram_freq=0)

    tboard.set_model( model2 )

    validate_gen = list_gen( bsize=batch_size, A_list_in=A_list_val, B_list_in=B_list_val, case_list_in=val_case_list, avg_slices=avg_slices, \
                            sh0=sh0, masks=masks, mm_pix=mm_pix, diam_rng=diam_rng, random_order=True, augment=False, prior_histogram=False)

    for ep in range(0,epochs):

    ## training

        model2.save_weights(model2_name)

        # view_weights(model=model2, layer_names=layer_names_part2, outpath=path)

        model2.fit_generator( generator=trainer_gen,
                             epochs=int( ep + 1 ),
                             steps_per_epoch=trainer_gen.__len__(),
                             verbose=1,
                             use_multiprocessing=True,
                             workers=4,
                             callbacks=[tboard], # ,reduce_lr],
                             initial_epoch=int( ep ),
                             validation_data=validate_gen,
                             validation_steps=validate_gen.__len__(),
                             validation_freq=5 )

    ## Radomize training batch order before next epoch

        trainer_gen.randomize_order()
        validate_gen.randomize_order() # doesn't matter, since the average gets reported ... randomize so we save different intermediate results

    ## Save weights

        model2.save_weights(model2_name)

        # temp = model2.get_weights()
        # np.save(path+'/model2_weights.npy',temp)

    ## Check results

        if ep == 0:

            B_mask_30_out  = np.transpose( B_mask_30, [1,2,0,3])

            B_mask_30_out = nib.Nifti1Image(B_mask_30_out, [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
            save_path = os.path.join(path, ('%04d_mask30.nii.gz') % ( ep+1 ))
            nib.save(B_mask_30_out, save_path)

            del B_mask_30_out


        if ( (ep+1) % 5 == 0 or ep < 5 ):

            # Change this to use the validation data?

            temp = model2.get_weights()
            np.save(path+'/weights' + str(ep+1) + '.npy',temp)

            Xt, Yt = validate_gen.__getitem__(0)

            XAB,XA,XB = model2.predict( Xt )

            X_A_in = Xt['X_A_in']
            X_B_in = Xt['X_B_in']
            mask20 = Xt['mask_20_in']

            Xin   = np.concatenate( (X_A_in,X_B_in), axis=-1 )
            Xout  = np.concatenate( (XA,XB), axis=-1 )

            XAB    = np.transpose( XAB ,  [1,2,0,3])
            Xin    = np.transpose( Xin ,  [1,2,0,3])
            Xout   = np.transpose( Xout,  [1,2,0,3])
            mask20 = np.transpose( mask20, [1,2,0,3])

            Xin = nib.Nifti1Image(Xin, [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
            save_path = os.path.join(path, ('%04d_Xin.nii.gz') % ( ep+1 ))
            nib.save(Xin, save_path)

            Xout = nib.Nifti1Image(Xout, [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
            save_path = os.path.join(path, ('%04d_Xout.nii.gz') % ( ep+1 ))
            nib.save(Xout, save_path)

            XAB = nib.Nifti1Image(XAB, [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
            save_path = os.path.join(path, ('%04d_XAB_out.nii.gz') % ( ep+1 ))
            nib.save(XAB, save_path)

            mask20 = nib.Nifti1Image(mask20, [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
            save_path = os.path.join(path, ('%04d_mask20.nii.gz') % ( ep+1 ))
            nib.save(mask20, save_path)
