
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from tensorflow import keras

from tensorflow.python.keras.optimizers import Optimizer

from tensorflow.python.keras import backend as K

from tensorflow.python.keras.initializers import Constant

from tensorflow.python.keras.layers import Layer

from scipy import ndimage as nd

import pydicom

import nibabel as nib

import numpy as np


## Support classes and functions

if True:

    # Stochastic optimizer (customized Adam optimizer from Keras)
    class AdamAccumulate_L4(Optimizer):

        def __init__(self, lr=0.001, beta_1=0.9, beta_2=0.999,
                     epsilon=1e-6, decay=0., amsgrad=False, accum_iters=1,
                     L2_decay=0, L2_decay_list=['kernel','bias'], # weight decay independent of Adam scaling/momentum
                     L1_decay=0, L1_t=1e-3, L1_decay_list=['kernel','bias'], # weight decay independent of Adam scaling/momentum
                     L4=False, zero_min_loss=False, alpha=0.15, gamma0=0.75, gamma=0.9, tau=1000, # L4 learning rate scaling method
                     **kwargs):
            #if accum_iters < 1:
            #    raise ValueError('accum_iters must be >= 1')
            super(AdamAccumulate_L4, self).__init__(**kwargs)
            with K.name_scope(self.__class__.__name__):
                self.iterations = K.variable(0, dtype='int64', name='iterations')
                self.lr = K.variable(lr, name='lr')
                self.beta_1 = K.variable(beta_1, name='beta_1')
                self.beta_2 = K.variable(beta_2, name='beta_2')
                self.decay = K.variable(decay, name='decay')
            if epsilon is None:
                epsilon = K.epsilon()
            self.epsilon = epsilon
            self.initial_decay = decay
            self.amsgrad = amsgrad
            self.accum_iters       = accum_iters
            # self.accum_iters       = K.variable(accum_iters, K.dtype(self.iterations))
            self.accum_iters_float = K.cast(self.accum_iters, K.floatx())
            # self.accum_iters_float = K.variable(accum_iters,  K.floatx())

            # weight decay independent of Adam scaling/momentum
            self.L2_decay      = L2_decay
            self.L2_decay_list = L2_decay_list

            # weight decay independent of Adam scaling/momentum
            self.L1_decay      = L1_decay
            self.L1_decay_list = L1_decay_list
            self.L1_t          = L1_t

            # L4 learning rate scaling method
            self.alpha         = alpha
            self.loss_min      = None
            self.gamma0        = gamma0
            self.gamma         = gamma
            self.tau           = tau
            self.L4            = L4
            self.zero_min_loss = zero_min_loss

        def reset_loss(self, loss):

            self.loss_min  = None

        # @interfaces.legacy_get_updates_support
        def get_updates(self, loss, params):
            grads = self.get_gradients(loss, params)
            self.updates = [K.update_add(self.iterations, 1)]

            # temp = K.get_value( self.lr )
            # print('Current LR in opt: ' + str(temp) )


        # original AdamAccumulate code

            # lr = self.lr
            #
            # completed_updates = K.cast(K.tf.floordiv(self.iterations, self.accum_iters), K.floatx())
            #
            # if self.initial_decay > 0:
            #     lr = lr * (1. / (1. + self.decay * completed_updates))
            #
            # t = completed_updates + 1
            #
            # lr_t = lr * (K.sqrt(1. - K.pow(self.beta_2, t)) / (1. - K.pow(self.beta_1, t)))

        # decay with resepect to overall iteration (as in the original Adam code) so we can change accum_iters without affecting the learning rate

            lr = self.lr
            if self.initial_decay > 0:
                lr = lr * (1. / (1. + self.decay * K.cast(self.iterations, K.dtype(self.decay))))

            t = K.cast(self.iterations, K.floatx()) + 1
            lr_t = lr * (K.sqrt(1. - K.pow(self.beta_2, t)) / (1. - K.pow(self.beta_1, t)))

        # Debugging code

            # lr = tf.Print(lr,[self.accum_iters, self.accum_iters_float],'AccumIters: ')

        # Use a binary flag to determine whether or not an update is applied

            # self.iterations incremented after processing a batch
            # batch:              1 2 3 4 5 6 7 8 9
            # self.iterations:    0 1 2 3 4 5 6 7 8
            # update_switch = 1:        x       x    (if accum_iters=4)
            update_switch = K.equal((self.iterations + 1) % self.accum_iters, 0)
            update_switch = K.cast(update_switch, K.floatx())

        # Continue with Adam code...

            ms = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
            vs = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
            gs = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]

            if self.amsgrad:
                vhats = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
            else:
                vhats = [K.zeros(1) for _ in params]

            self.weights = [self.iterations] + ms + vs + vhats

            for p, g, m, v, vhat, tg in zip(params, grads, ms, vs, vhats, gs):

                sum_grad = tg + g
                avg_grad = sum_grad / self.accum_iters_float

                m_t = (self.beta_1 * m) + (1. - self.beta_1) * avg_grad
                v_t = (self.beta_2 * v) + (1. - self.beta_2) * K.square(avg_grad)

                if self.amsgrad:
                    vhat_t = K.maximum(vhat, v_t)
                    p_t = p - lr_t * m_t / (K.sqrt(vhat_t) + self.epsilon)
                    self.updates.append(K.update(vhat, (1 - update_switch) * vhat + update_switch * vhat_t))
                elif self.L4:# L4 adaptive gradient method

                    p_t = m_t / (K.sqrt(v_t) + self.epsilon)

                    if self.loss_min is not None:

                        self.loss_min = K.minimum( self.loss_min, loss )

                    else:

                        self.loss_min = self.gamma0 * loss

                    if self.zero_min_loss: # convergence of positive and negative costs to zero expected

                        # eta = self.alpha *  K.abs( loss ) / ( K.sum( p_t * m_t ) + self.epsilon )

                        # eta = self.alpha * ( 1.0 / ( K.abs( loss ) + 1 ) ) *  K.sqrt( K.sum( avg_grad**2 ) *  K.sum( m**2 ) ) / ( K.sum( avg_grad * m ) + self.epsilon )

                        eta = lr_t * ( 1.0 / ( K.abs( loss )**self.alpha + 1 ) ) *  K.sqrt( K.sum( avg_grad**2 ) *  K.sum( m**2 ) ) / ( K.sum( avg_grad * m ) + self.epsilon )

                    else: # convergence of positive costs

                        eta = self.alpha * ( loss - self.gamma * self.loss_min ) / ( K.sum( p_t * m_t ) + self.epsilon )

                    p_t = p - eta * p_t

                    self.loss_min *= (1 + 1/self.tau)

                else:

                    p_t = p - lr_t * m_t / (K.sqrt(v_t) + self.epsilon)

            # Weight decay
            # Only applied to trainable parameters with name 'kernel' or 'bias' (by default).

                if any([weight_name_str in p.name for weight_name_str in self.L2_decay_list ]):

                    p_t -= self.L2_decay * lr_t * p

                if any([weight_name_str in p.name for weight_name_str in self.L1_decay_list ]):
                    # https://en.wikipedia.org/wiki/Huber_loss
                    # Charbonnier, P., Blanc-Féraud, L., Aubert, G., & Barlaud, M. (1997).
                    # Deterministic edge-preserving regularization in computed imaging.
                    # IEEE Transactions on image processing, 6(2), 298-311.
                    # L2 near zero, L1 away from zero... smaller self.L1_t values = transition closer to zero

                    # p_t -= self.L1_decay * lr_t * ( ( self.L1_t * p )  / K.sqrt( 1 + ( p / self.L1_t )**2 ) )

                    # away from zero, magnitude is set by self.L1_decay * lr_t, because the derivative is normalized to +/- 1
                    # derivative smoothly transitions between +/- self.L1_t
                    p_t -= self.L1_decay * lr_t * ( ( p / self.L1_t )  / K.sqrt( 1 + ( p / self.L1_t )**2 ) )

            # Conditional parameter update for accumulation

                self.updates.append(K.update(m, (1 - update_switch) * m + update_switch * m_t))
                self.updates.append(K.update(v, (1 - update_switch) * v + update_switch * v_t))
                self.updates.append(K.update(tg, (1 - update_switch) * sum_grad))
                new_p = p_t

                # Apply constraints.
                if getattr(p, 'constraint', None) is not None:
                    new_p = p.constraint(new_p)

                self.updates.append(K.update(p, (1 - update_switch) * p + update_switch * new_p))
            return self.updates

        def get_config(self):
            config = {'lr': float(K.get_value(self.lr)),
                      'beta_1': float(K.get_value(self.beta_1)),
                      'beta_2': float(K.get_value(self.beta_2)),
                      'decay': float(K.get_value(self.decay)),
                      'epsilon': self.epsilon,
                      'amsgrad': self.amsgrad,
                      'accum_iters': self.accum_iters,
                      'L2_decay':self.L2_decay,
                      'L2_decay_list':self.L2_decay_list,
                      'L1_decay':self.L1_decay,
                      'L1_t':self.L1_t,
                      'L1_decay_list':self.L1_decay_list,
                      'L4':self.L4,
                      'alpha':self.alpha,
                      'gamma0':self.gamma0,
                      'gamma':self.gamma,
                      'tau':self.tau,
                      'zero_min_loss':self.zero_min_loss}

            base_config = super(AdamAccumulate_L4, self).get_config()
            return dict(list(base_config.items()) + list(config.items()))


    # Keras data generator
    class list_gen(keras.utils.Sequence):

        'Generates data for Keras'

        def __init__(self, bsize, A_list_in, B_list_in, case_list_in, avg_slices, sh0, masks, mm_pix, diam_rng, random_order=False, augment=False, override_mask=False, omask=None, nifti_source=None, prior_histogram=False):

            self.prior_histogram = prior_histogram

            if self.prior_histogram:

                ref_hist_file = '/media/x-ray/blkbeauty4/Duke_S_Extrap_2019_Results/Ref_histogram.npy'
                _,self.valid_combos_A,self.valid_combos_B = np.load( ref_hist_file )

            self.bsize = bsize

            self.avg_slices = avg_slices

            self.A_list_f  = []     # all chain A files in one list
            self.B_list_f  = []     # all chain B files in one list
            self.case_list = []     # string 'Case #' for all files in the A and B lists
            self.current_cases = [] # list of cases associated with the current batch; requires serial execution!

            # Count the total number of slices
            self.tslices = 0

            self.sh0 = sh0

            self.masks    = masks
            self.mm_pix   = mm_pix
            self.diam_rng = diam_rng

        # Process the data with a static ~20 cm mask

            self.override_mask = override_mask
            self.omask = omask

            self.nifti_source = nifti_source

            if nifti_source is not None:

                self.data = nib.load(nifti_source).get_data().astype('float32')

                # make sure the data shape is divisible by the batch size, so all data will be processed
                assert self.bsize * ( self.data.shape[-1] // self.bsize ) == self.data.shape[-1]

                self.total_batches = self.data.shape[-1] // self.bsize

            else:

            ## Skip slices so we stay divisible by the number of averaged slices, avoid averaging mis-matched slices
            ## Note: Only full batches are processed.

                for i in range(0,len(A_list_in)):

                    sz = ( len(A_list_in[i]) // self.avg_slices ) * self.avg_slices

                    self.A_list_f  += A_list_in[i][slice(0,sz)]
                    self.B_list_f  += B_list_in[i][slice(0,sz)]

                    self.case_list += case_list_in[i][slice(0,sz)]

                    self.tslices += sz

                self.total_batches = self.tslices // ( self.bsize * self.avg_slices )

        ## Go through all slices in order, or randomize the order?
        ## Only average consecutive slices.

            self.random_order = random_order
            self.augment      = augment

            if self.random_order:

                self.order = np.random.permutation( self.total_batches )

            else:

                self.order = np.arange(0,self.total_batches)


        def __len__(self):

            'Denotes the number of batches per epoch'
            return self.total_batches


        def change_override_mask(self,override_mask=False, omask=None):

            self.override_mask = override_mask

            self.omask = omask


        def get_cases(self):

            return self.current_cases


        def randomize_order(self):

            self.order = np.random.permutation( self.total_batches )


        def __getitem__(self, index):

            while True:

                # load and average
                out_img_A   = np.zeros( (self.bsize,self.sh0[0],self.sh0[1],1), dtype=np.float32 )
                out_img_B   = np.zeros( (self.bsize,self.sh0[0],self.sh0[1],1), dtype=np.float32 )

            ## Choose data

                # mask_20_out = np.copy( B_mask_20 )

                c_slice = self.bsize * self.order[index] * self.avg_slices

                bc = 0

                self.current_cases = []

                for i in range(c_slice, c_slice + self.bsize * self.avg_slices, self.avg_slices):

                    out_img_A_ = 0
                    out_img_B_ = 0

                    self.current_cases.append( self.case_list[i] )

                    for j in range(0,self.avg_slices):

                        temp        = pydicom.dcmread(self.A_list_f[i+j])
                        img         = temp.pixel_array
                        out_img_A_ += ( img.astype('float32') * float(temp.RescaleSlope) + float( temp.RescaleIntercept ) )

                        temp        = pydicom.dcmread(self.B_list_f[i+j])
                        img         = temp.pixel_array
                        out_img_B_ += ( img.astype('float32') * float(temp.RescaleSlope) + float( temp.RescaleIntercept ) )

                    out_img_A[bc,...,0] = out_img_A_ / self.avg_slices
                    out_img_B[bc,...,0] = out_img_B_ / self.avg_slices

                    bc += 1

            ## Apply flips

                if self.augment:

                    doflip = np.random.randint(4)

                    # assumes the masks are circularly symmetric and do not need to be flipped
                    if doflip == 1:

                        out_img_A  = np.flip( out_img_A, axis=2 )
                        out_img_B  = np.flip( out_img_B, axis=2 )

                    elif doflip == 2:

                        out_img_A  = np.flip( out_img_A, axis=1 )
                        out_img_B  = np.flip( out_img_B, axis=1 )

                    elif doflip == 3:

                        out_img_A  = np.flip( out_img_A, axis=1 )
                        out_img_B  = np.flip( out_img_B, axis=1 )

                        out_img_A  = np.flip( out_img_A, axis=2 )
                        out_img_B  = np.flip( out_img_B, axis=2 )

            ## Rotate

                if self.augment:

                    theta1 = 180*(np.random.rand()-0.5)/0.5

                    out_img_A = nd.rotate(out_img_A, angle=theta1, axes=[1,2], reshape=False, cval=-1000)
                    out_img_B = nd.rotate(out_img_B, angle=theta1, axes=[1,2], reshape=False, cval=-1000)

                    # out_img_A /= 1000
                    # out_img_B /= 1000

            ## Intensity scaling

                # air => 0, water => 1
                out_img_A = (out_img_A + 1000)/1000
                out_img_B = (out_img_B + 1000)/1000

            ## Choose mask sizes

                if self.override_mask==False:

                    Isum = np.sum( out_img_A, axis=(1,2,3) )

                    # b,x,y,1 * x,y,11 => b,x,y,11 => b,11
                    Msum = np.sum( out_img_A * self.masks, axis=(1,2) )

                    # Midx = int( np.sum( ( ( Msum / Isum[:,np.newaxis] ) < 0.50 ), axis=1 ) - 1 )

                    # Midx = np.sum( ( ( Msum / Isum[:,np.newaxis] ) < 0.30 ), axis=1 ) - 1

                    # Midx = np.sum( ( ( Msum / Isum[:,np.newaxis] ) < 0.40 ), axis=1 ) - 1

                    Midx = np.sum( ( ( Msum / Isum[:,np.newaxis] ) < 0.35 ), axis=1 ) - 1

                    # x,y,11 => b,x,y,1
                    mask_20_out = np.transpose( self.masks[np.newaxis,:,:,Midx], (3,1,2,0) )
                    diams = self.diam_rng[Midx]

            ## Translation of the 20 cm mask only

                # self.override_mask = override_mask
                # self.omask = omask

                if self.augment and self.override_mask==False:

                    for i in range(0,self.bsize):

                        # choose random polar shift
                        theta = 3.1415926535 * (np.random.rand()-0.5)/0.5 # +/- pi radians
                        # r     =           50 * (np.random.rand()-0.5)/0.5 # max_rad = +/- 10/2 cm
                        r     = ( ( 300 - diams[i] ) // 2 ) * (np.random.rand()-0.5)/0.5 # stay within 30 cm mask
                        r     /= self.mm_pix # convert from mm to pixels

                        # convert to x,y, truncate to integer
                        trans_x = int( r * np.cos( theta ) )
                        trans_y = int( r * np.sin( theta ) )

                        # apply translation
                        mask_20_out[i,...] = np.roll( mask_20_out[i,...], trans_x, axis=1 )
                        mask_20_out[i,...] = np.roll( mask_20_out[i,...], trans_y, axis=2 )

                elif self.override_mask==True:

                    mask_20_out = np.tile( self.omask[np.newaxis,...,np.newaxis], [self.bsize,1,1,1] )

            ## If we are using a prior histogram, replace the first sample

                if self.prior_histogram:

                    idx = np.random.randint( low=0, high=self.valid_combos_A.shape[0] )

                    IA = self.valid_combos_A[idx]
                    IB = self.valid_combos_B[idx]

                    out_img_A[slice(0,1),...] = out_img_A[slice(0,1),...] * 0.0 + IA + 0.03 * np.random.normal( size=(1,sh0[0],sh0[1],1) )
                    out_img_B[slice(0,1),...] = out_img_B[slice(0,1),...] * 0.0 + IB + 0.03 * np.random.normal( size=(1,sh0[0],sh0[1],1) )

            ## Output

                out_zeros = np.zeros( (self.bsize,), dtype=np.float32 )

                # return {'X_A_in':out_img_A,'X_B_in':out_img_B,'mask_20_in':mask_20_out}, \
                #        {'Breg_cost':out_zeros, 'Breg_costB':out_zeros, 'XAB_out':out_img_B, 'XA_RMSE':out_img_A, 'XA0_RMSE':out_img_A, 'XB_RMSE':out_img_B, 'XB0_RMSE':out_img_B, 'XB_avg':out_img_B }

                return {'X_A_in':out_img_A,'X_B_in':out_img_B,'mask_20_in':mask_20_out}, \
                       {'XAB_out':out_img_B, 'XA_RMSE':out_img_A, 'XB_RMSE':out_img_B }


    # Custom piece-wise linear transfer function layer with learnable parameters
    class PiecewiseLinearTransfer(Layer):

        def __init__(self, grid, min_val=-1e6, max_val=1e6, knots=10, **kwargs):

            self.min_val = min_val
            self.max_val = max_val
            self.knots   = knots
            self.grid    = grid

            super().__init__(**kwargs)

        def build(self, input_shape):

            self.coeffs = self.add_weight(name='pw_linear_coeffs',
                                          shape=(self.knots,),
                                          initializer=Constant( self.grid ),
                                          trainable=True)

            super().build(input_shape)  # Be sure to call this at the end

        def call(self, X):

            # X   : b,1
            # grid: k
            # interval: b
            # interp: b,self.knots-1

            interval = tf.argmax( tf.cast( tf.logical_and( tf.greater( X, self.grid[0:self.knots-1] ),  tf.less( X, self.grid[1:self.knots] ) ), tf.float32), axis=-1 )

            a = ( X - self.grid[0:self.knots-1] ) / ( self.grid[1:self.knots] -  self.grid[0:self.knots-1] )
            b = ( self.grid[1:self.knots] - X ) / ( self.grid[1:self.knots] -  self.grid[0:self.knots-1] )

            interp = a * self.coeffs[1:self.knots] + b * self.coeffs[0:self.knots-1]

            interp   = tf.reshape(interp,[-1])
            interval = tf.cast( interval, tf.int32 ) + tf.range(0,tf.shape(X)[0]) * (self.knots-1)

            X_out = tf.gather( interp, interval )

            X_out = tf.reshape(X_out,tf.shape(X))

            return X_out

        def compute_output_shape(self, input_shape):

            return input_shape


    # Binary mask for selecting image regions
    def make_diam_rmask( nx, ny, diam, batch_size ):

        rmask = np.zeros( (nx,ny), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1 ),
                              np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1), indexing='ij' )

        rmask[ np.sqrt( np.add(x1**2, x2**2) ) <= diam/2.0 ] = 1

        rmask = rmask[np.newaxis,:,:,np.newaxis]

        rmask = np.tile( rmask, [batch_size,1,1,1] )

        return rmask


    # Merge estimated chain B data with true chain B data
    def fill_B(X_B,X_A_out,mask):

        X_B_out = (1-mask) * X_A_out + mask * X_B

        return X_B_out


    # Root-mean-squared error cost, all data
    def RMSE(y_true,y_pred):

        # assumes the mask is replicated over the batch size
        RMSE = tf.sqrt( tf.reduce_mean( ( y_true - y_pred )**2 ) )

        return RMSE


    # RMSE computed with a mask only
    def masked_RMSE(y_true,y_pred,mask):

        global sh0, batch_size

        # multiply the mask by the number of output Bregman iterations to compensate for broadcasting
        ch_1 = tf.cast( tf.shape(y_pred)[-1], dtype=tf.float32 )

        # assumes the mask is replicated over the batch size
        RMSE = tf.sqrt( tf.reduce_sum( mask * ( y_true - y_pred )**2 ) / ( ch_1 * tf.reduce_sum(mask) ) )

        return RMSE


    # 2D Gaussian smoothing kernel implemented with separable, 1D convolutions
    def make_G_2D_1D( FWHM=2.0, w=7 ):

        rad = int((w-1)/2)

        sigma = FWHM / ( 2 * np.sqrt(2 * np.log(2) ) )
        Gx = np.zeros((w,1,1,1),dtype=np.float32)
        Gy = np.zeros((1,w,1,1),dtype=np.float32)
        for i in range(-rad,rad+1):

            Gx[i+rad,...]   = np.exp( -( i**2 ) / ( 2 * ( sigma**2 ) ) )
            Gy[:,i+rad,...] = np.exp( -( i**2 ) / ( 2 * ( sigma**2 ) ) )

        Gx = Gx / np.sum(Gx)
        Gy = Gy / np.sum(Gy)

        return Gx, Gy
